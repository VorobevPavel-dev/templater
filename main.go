package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

var (
	template_type    string
	useDotFile       bool
	allowedTemplates []string = []string{"gitlab_failure"}
)

func contains[T comparable](elems []T, v T) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func toStr(elems []string) string {
	return "[" + strings.Join(elems[:], ",") + "]"
}

type GitlabFailure struct {
	Project string
	Stage   string
	Stand   string
	URL     string

	templateFile string
}

func (g *GitlabFailure) File() string {
	return g.templateFile
}

func (g *GitlabFailure) Render() {
	f, err := os.ReadFile(g.templateFile)
	if err != nil {
		log.Fatalf("file %s does not exist", g.templateFile)
	}
	gtpl := template.Must(template.New("Message").Parse(string(f)))
	err = gtpl.Execute(os.Stdout, &g)
	if err != nil {
		panic(err)
	}
}

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Simple CLI utility to generate Telegram messages from Golang templates and env variables.\n\nUsage of %s\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.StringVar(&template_type, "template", "gitlab_failure", fmt.Sprintf("Template to use. Allowed: %s", toStr(allowedTemplates)))
	flag.BoolVar(&useDotFile, "use-env-file", false, "Load vars from local .env file")
	flag.Parse()

	if !contains(allowedTemplates, template_type) {
		log.Fatalf("template %s is not allowed yet", template_type)
	}
	if useDotFile {
		if _, err := os.Stat(".env"); err != nil {
			if os.IsNotExist(err) {
				log.Fatal("file .env does not exist")
			}
		}
	}
}

func main() {
	// Load env vars if needed
	if useDotFile {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("error loading .env file")
		}
	}

	// Determine struct to load and parse template
	switch template_type {
	case "gitlab_failure":
		resultTemplate := GitlabFailure{
			templateFile: "templates/gitlab_failure.gtpl",
			Project:      os.Getenv("PROJECT"),
			Stage:        os.Getenv("STAGE"),
			Stand:        os.Getenv("STAND"),
			URL:          os.Getenv("URL"),
		}
		resultTemplate.Render()
	}
}
