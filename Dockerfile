FROM golang@sha256:70df3b8f9f099da7f60f0b32480015165e3d0b51bfacf9e255b59f3dd6bd2828 as builder

ENV GOOS=linux
ENV GOARCH=amd64
ENV USER=appuser
ENV UID=10001

RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}" 

WORKDIR /app
COPY . .
RUN go get -d -v && go build -ldflags="-w -s" -o /go/bin/templater

FROM alpine@sha256:1304f174557314a7ed9eddb4eab12fed12cb0cd9809e4c28f29af86979a3c870
USER nonexist
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /go/bin/templater /templater
COPY templates /templates
USER appuser:appuser
ENTRYPOINT ["/templater"]