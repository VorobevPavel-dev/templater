PROJECT_NAME=templater

all: build

build:
	go build -o bin/${PROJECT_NAME} .

clean:
	rm -dfr bin

podman:
	podman build . -t pvorobev2/templater:v0.1

docker:
	docker build . -t pvorobev2/templater:v0.1

zip:
	zip -r templater.zip go.mod go.sum main.go templates